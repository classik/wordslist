<?php

namespace Api\ApiBundle\Helpers;


class Token
{
    /**
     * @var
     */
    private $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return $this
     */
    public function generate($time = '+1 year')
    {
        $date = new \DateTime($time);
        $token = ['token' => bin2hex(openssl_random_pseudo_bytes(15)), 'ttl' => $date->getTimestamp()];
        $this->setToken($token);

        return $this;
    }

    /**
     * @param $token
     * @return bool
     */
    public function isExpireToken($token)
    {
        return (isset($token['ttl']) && $token['ttl'] < time());
    }
}