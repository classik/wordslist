<?php

namespace Api\ApiBundle\Helpers;

use Symfony\Component\HttpFoundation\Request;


class Hash
{
    /**
     *
     */
    const SECRET_KEY = '3umnY3Mg';

    /**
     * @var Request
     */
    private $request;

    /**
     * Hash constructor.
     * @param Request|null $request
     */
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    private function calculateHash()
    {
        $params = $this->request->request->all();
        if (isset($params['hash'])) {
            unset($params['hash']);
        }

        $hash = $this->makeHash($params);

        return $hash;
    }

    /**
     * @param $params
     * @return string
     */
    public function makeHash($params)
    {
        ksort($params);

        return sha1(implode(',', $params) . self::SECRET_KEY);
    }

    /**
     * @return bool
     */
    public function isMatchHash()
    {
        return true;

        $hash = $this->request->request->get('hash', null);
        $calculatedHash = $this->calculateHash();

        return $hash === $calculatedHash;
    }
}