<?php

namespace Api\ApiBundle\Document;


use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="users")
 */
class User
{
    /**
     *
     */
    const STATUS_ACTIVE = 'active';

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 'not_active';

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $first_name;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $last_name;

    /**
     * @MongoDB\Field(type="collection")
     */
    protected $tokens;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $password;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isFacebook;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $link;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $picture;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set tokens
     *
     * @param collection $tokens
     * @return $this
     */
    public function setTokens($tokens)
    {
        $this->tokens[] = $tokens;
        return $this;
    }

    /**
     * Get tokens
     *
     * @return collection $tokens
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @return mixed
     */
    public function getLastToken()
    {
        return $this->tokens[count($this->tokens) - 1];
    }

    /**
     * @param $token
     * @return null
     */
    public function getTtlByToken($token)
    {
        $ttl = null;

        foreach ($this->tokens as $tkn) {
            if ($tkn['token'] === $token) {
                return $tkn['ttl'];
            }
        }

        return $ttl;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $password = password_hash($password, PASSWORD_BCRYPT);
        $this->password = $password;
        return $this;
    }

    /**
     * @param $password
     * @return bool
     */
    public function verifyPassword($password)
    {
        return password_verify($password, $this->getPassword());
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isFacebook
     *
     * @param boolean $isFacebook
     * @return $this
     */
    public function setIsFacebook($isFacebook)
    {
        $this->isFacebook = $isFacebook;
        return $this;
    }

    /**
     * Get isFacebook
     *
     * @return boolean $isFacebook
     */
    public function getIsFacebook()
    {
        return $this->isFacebook;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return $this
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * Get picture
     *
     * @return string $picture
     */
    public function getPicture()
    {
        return $this->picture;
    }
}
