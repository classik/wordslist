<?php

namespace Api\ApiBundle\Document;


use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="study_set")
 */
class StudySet
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="object_id")
     */
    protected $userId;

    /**
     * @MongoDB\Field(type="collection")
     */
    protected $words;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $lang;

    /**
     * @MongoDB\Field(type="timestamp")
     */
    protected $created;

    /**
     * @MongoDB\Field(type="timestamp")
     */
    protected $updated;

    /**
     * @MongoDB\Field(type="timestamp")
     */
    protected $deleted;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set words
     *
     * @param collection $words
     * @return $this
     */
    public function setWords($words)
    {
        $this->words = $words;
        return $this;
    }

    /**
     * Get words
     *
     * @return collection $words
     */
    public function getWords()
    {
        return $this->words;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return $this
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * Get lang
     *
     * @return string $lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set created
     *
     * @param timestamp $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return timestamp $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param timestamp $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Get updated
     *
     * @return timestamp $updated
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deleted
     *
     * @param timestamp $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * Get deleted
     *
     * @return timestamp $deleted
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set userId
     *
     * @param object_id $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return object_id $userId
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
