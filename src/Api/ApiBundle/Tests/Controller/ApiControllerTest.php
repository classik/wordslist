<?php

namespace Api\ApiBundle\Test\Controller;


use Api\ApiBundle\DataFixtures\MongoDB\LoadUserData;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Api\ApiBundle\Helpers\Hash;
use Api\ApiBundle\Document\User;
use Api\ApiBundle\Codes;

class ApiControllerTest extends WebTestCase
{
    protected $client;
    protected $entityManager;

    /**
     *
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $container = $this->client->getContainer();
        $doctrine = $container->get('doctrine_mongodb');
        $this->entityManager = $doctrine->getManager();
        $fixture = new LoadUserData();
        $fixture->load($this->entityManager);
    }

    /**
     * Test api/register-user with exists user in DB
     */
    public function testRegisterUserExistsUser()
    {
        $user = LoadUserData::$users['new_user'];
        $userToken = $user->getLastToken();

        $params = [
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'facebook' => $user->getIsFacebook(),
            'link' => $user->getLink()
        ];

        $hash = new Hash();
        $userHash = $hash->makeHash($params);
        $params['hash'] = $userHash;

        $this->client->request('POST', '/api/register-user', $params);

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('data', $response['response']);
        $this->assertArrayHasKey('token', $response['response']['data']);
        $this->assertArrayHasKey('user_id', $response['response']['data']);
        $this->assertEquals($user->getId(), $response['response']['data']['user_id']);
        $this->assertEquals($userToken['token'], $response['response']['data']['token']);
    }

    /**
     * Test api/register-user with exists user in DB and expired token
     */
    public function testRegisterUserExpiredUserToken()
    {
        $user = LoadUserData::$users['new_user_expired_token'];
        $expiredToken = $user->getLastToken();

        $params = [
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'facebook' => $user->getIsFacebook(),
            'link' => $user->getLink()
        ];

        $hash = new Hash();
        $userHash = $hash->makeHash($params);
        $params['hash'] = $userHash;

        $this->client->request('POST', '/api/register-user', $params);

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $userToken = $user->getLastToken();
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('data', $response['response']);
        $this->assertArrayHasKey('token', $response['response']['data']);
        $this->assertArrayHasKey('user_id', $response['response']['data']);
        $this->assertEquals($user->getId(), $response['response']['data']['user_id']);
        $this->assertNotEquals($expiredToken['token'], $response['response']['data']['token']);
        $this->assertEquals($userToken['token'], $response['response']['data']['token']);
    }

    /**
     * Test api/register-user with not exists user in DB
     */
    public function testRegisterUserNotExistsUser()
    {
        $firstName = uniqid('TestFirstName');
        $lastName = uniqid('TestLastName');
        $email = uniqid('test') . '@' . 'gmail.com';
        $password = uniqid('pass');
        $status = User::STATUS_ACTIVE;
        $isFacebook = true;
        $link = uniqid('facebook_link') . '@' . 'facebook.com';
        $picture = $link . '/img.jpg';

        $params = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => $password,
            'facebook' => $isFacebook,
            'link' => $link,
            'picture' => $picture,
            'status' => $status
        ];

        $hash = new Hash();
        $userHash = $hash->makeHash($params);
        $params['hash'] = $userHash;

        $this->client->request('POST', '/api/register-user', $params);

        $user = $this->entityManager
            ->getRepository('ApiApiBundle:User')
            ->findOneByEmail($email);

        $userToken = $user->getLastToken();

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('data', $response['response']);
        $this->assertArrayHasKey('token', $response['response']['data']);
        $this->assertArrayHasKey('user_id', $response['response']['data']);
        $this->assertEquals($user->getId(), $response['response']['data']['user_id']);
        $this->assertEquals($userToken['token'], $response['response']['data']['token']);

        $this->entityManager->remove($user);
    }

    /**
     * Test api/ping
     */
    public function testPing()
    {
        $this->client->request('POST', '/api/ping');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test api/signin not exists user
     */
    public function testSignInUserNotExists()
    {
        $email = uniqid('test') . '@' . 'gmail.com';
        $password = uniqid('pass');
        $params = ['email' => $email, 'password' => $password];

        $this->client->request('POST', '/api/signin', $params);

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(is_array($response));
        $this->assertArrayHasKey('status_code', $response);
        $this->assertEquals(400, $response['status_code']);
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('code', $response['response']);
        $this->assertEquals(Codes::CODE_702, $response['response']['code']);
        $this->assertEquals('User not exists', $response['response']['message']);
    }

    /**
     * Test api/signin exists user
     */
    public function testSignInUserExists()
    {
        $user = LoadUserData::$users['new_user_not_facebook'];
        $params = ['email' => $user->getEmail(), 'password' => LoadUserData::$defaultPassword];

        $this->client->request('POST', '/api/signin', $params);

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(is_array($response));
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('data', $response['response']);
        $this->assertArrayHasKey('token', $response['response']['data']);
        $this->assertArrayHasKey('user_id', $response['response']['data']);
        $this->assertEquals($user->getId(), $response['response']['data']['user_id']);
        $userToken = $user->getLastToken();
        $this->assertEquals($userToken['token'], $response['response']['data']['token']);
    }

    /**
     * Test api/signin facebook user
     */
    public function testSignInFacebookUser()
    {
        $user = LoadUserData::$users['new_user'];
        $params = ['email' => $user->getEmail(), 'password' => LoadUserData::$defaultPassword];

        $this->client->request('POST', '/api/signin', $params);

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(is_array($response));
        $this->assertArrayHasKey('status_code', $response);
        $this->assertEquals(400, $response['status_code']);
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('code', $response['response']);
        $this->assertEquals(Codes::CODE_703, $response['response']['code']);
        $this->assertEquals('You were login with Facebook', $response['response']['message']);
    }

    /**
     * Test api/signin exists user wrong password
     */
    public function testSignInExistsUserWrongPassword()
    {
        $user = LoadUserData::$users['new_user_not_facebook'];
        $params = ['email' => $user->getEmail(), 'password' => uniqid('pass')];

        $this->client->request('POST', '/api/signin', $params);

        $this->assertStatusCode(200, $this->client);
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(is_array($response));
        $this->assertArrayHasKey('status_code', $response);
        $this->assertEquals(400, $response['status_code']);
        $this->assertArrayHasKey('response', $response);
        $this->assertArrayHasKey('code', $response['response']);
        $this->assertEquals(Codes::CODE_701, $response['response']['code']);
        $this->assertEquals('Wrong password', $response['response']['message']);
    }

    /**
     *
     */
    public function tearDown()
    {
        $this->entityManager->remove(LoadUserData::$users['new_user']);
        $this->entityManager->remove(LoadUserData::$users['new_user_expired_token']);
        $this->entityManager->remove(LoadUserData::$users['new_user_not_facebook']);
        $this->entityManager->flush();
    }
}