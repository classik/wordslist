<?php

namespace Api\ApiBundle\Test\Document;


use Api\ApiBundle\Document\User;
use Api\ApiBundle\Helpers\Token;

// run test vendor/bin/phpunit -c app/
class UserTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatingUser()
    {
        $user = new User();
        $token = new Token();

        $firstName = 'Valik';
        $lastName = 'Kremeshnyi';
        $userToken = $token->generate()->getToken();
        $email = uniqid('test') . '@' . 'gmail.com';
        $password = uniqid('pass');
        $status = User::STATUS_ACTIVE;
        $isFacebook = true;
        $link = uniqid('facebook_link') . '@' . 'facebook.com';
        $picture = $link . '/img.jpg';

        $user->setFirstName($firstName)
            ->setLastName($lastName)
            ->setTokens($userToken)
            ->setEmail($email)
            ->setPassword($password)
            ->setStatus($status)
            ->setIsFacebook($isFacebook)
            ->setLink($link)
            ->setPicture($picture);

        $this->assertEquals($firstName, $user->getFirstName());
        $this->assertEquals($lastName, $user->getLastName());
        $this->assertEquals($userToken, $user->getLastToken());
        $this->assertEquals($email, $user->getEmail());
        $this->assertTrue($user->verifyPassword($password));
        $this->assertEquals($status, $user->getStatus());
        $this->assertEquals($isFacebook, $user->getIsFacebook());
        $this->assertEquals($link, $user->getLink());
        $this->assertEquals($picture, $user->getPicture());
    }

    public function testSetTokens()
    {
        $user = new User();
        $token = new Token();

        $token1 =$token->generate()->getToken();
        $token2 =$token->generate()->getToken();

        $user->setTokens($token1);
        $user->setTokens($token2);

        $this->assertEquals($token2, $user->getLastToken());
    }
}
