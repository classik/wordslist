<?php

namespace Api\ApiBundle\DataFixtures\MongoDB;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Api\ApiBundle\Document\User;
use Api\ApiBundle\Helpers\Token;

/**
 * Class LoadUserData
 * @package Api\ApiBundle\DataFixtures\MongoDB
 */
class LoadUserData extends AbstractFixture
{
    /**
     * @var array
     */
    public static $users = array();

    /**
     * @var string
     */
    public static $defaultPassword = 'qwerty';

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user2 = new User();
        $user3 = new User();
        $token = new Token();

        // User 1
        $firstName = uniqid('TestFirstName');
        $lastName = uniqid('TestLastName');
        $userToken = $token->generate()->getToken();
        $email = uniqid('test') . '@' . 'gmail.com';
        $password = uniqid('pass');
        $status = User::STATUS_ACTIVE;
        $isFacebook = true;
        $link = uniqid('facebook_link') . '@' . 'facebook.com';
        $picture = $link . '/img.jpg';

        $user1->setFirstName($firstName)
            ->setLastName($lastName)
            ->setTokens($userToken)
            ->setEmail($email)
            ->setPassword($password)
            ->setStatus($status)
            ->setIsFacebook($isFacebook)
            ->setLink($link)
            ->setPicture($picture);

        // User 2
        $firstName = uniqid('TestFirstName');
        $lastName = uniqid('TestLastName');
        $userToken = $token->generate('-1 year')->getToken();
        $email = uniqid('test') . '@' . 'gmail.com';
        $password = uniqid('pass');
        $status = User::STATUS_ACTIVE;
        $isFacebook = true;
        $link = uniqid('facebook_link') . '@' . 'facebook.com';
        $picture = $link . '/img.jpg';

        $user2->setFirstName($firstName)
            ->setLastName($lastName)
            ->setTokens($userToken)
            ->setEmail($email)
            ->setPassword($password)
            ->setStatus($status)
            ->setIsFacebook($isFacebook)
            ->setLink($link)
            ->setPicture($picture);

        // User 3
        $firstName = uniqid('TestFirstName');
        $lastName = uniqid('TestLastName');
        $userToken = $token->generate()->getToken();
        $email = uniqid('test') . '@' . 'gmail.com';
        $password = self::$defaultPassword;
        $status = User::STATUS_ACTIVE;
        $isFacebook = false;
        $link = '';
        $picture = '';

        $user3->setFirstName($firstName)
            ->setLastName($lastName)
            ->setTokens($userToken)
            ->setEmail($email)
            ->setPassword($password)
            ->setStatus($status)
            ->setIsFacebook($isFacebook)
            ->setLink($link)
            ->setPicture($picture);

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->flush();

        self::$users['new_user'] = $user1;
        self::$users['new_user_expired_token'] = $user2;
        self::$users['new_user_not_facebook'] = $user3;
    }
}